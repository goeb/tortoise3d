# This file is part of the Tortoise3D package for Python.
#
# Tortoise3D is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Tortoise3D is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Tortoise3D. If not,
# see <http://www.gnu.org/licenses/>.
#
# Copyright 2014 Stefan Goebel <tortoise3d /at\ subtype \dot/ de>

import os
from setuptools import setup, find_packages, Command

def read (fname):
   return open (os.path.join (os.path.dirname (__file__), fname)).read ()

setup (
   name             = 'Tortoise3D',
   version          = '0.4',
   author           = 'Stefan Goebel',
   author_email     = 'tortoise3d@subtype.de',
   description      = 'Like 3D turtle graphics, but without the graphics part.',
   license          = 'GPLv3',
   keywords         = '3d tortoise turtle quaternion',
   url              = 'https://gitlab.com/goeb/tortoise3d',
   packages         = find_packages (),
   long_description = read ('README'),
   classifiers      = [
      'Development Status :: 4 - Beta',
      'Topic :: Software Development :: Libraries',
      'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
   ],
   zip_safe         = True,
)

# :indentSize=3:tabSize=3:noTabs=true:mode=python:maxLineLen=100: