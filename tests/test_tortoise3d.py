# coding=utf-8

# This file is part of the Tortoise3D package for Python.
#
# Tortoise3D is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# Tortoise3D is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Tortoise3D. If not, see <http://www.gnu.org/licenses/>.
#
# Copyright 2014 Stefan Goebel <tortoise3d /at\ subtype \dot/ de>

# Note: Test cases are very basic. For example, rotations are only
# tested for 90°, 180° or 360°. Movement is only tested for simple
# integer values.

import math
import pytest
import random
import sys

sys.path.append ('.')
from tortoise3d import Tortoise3D as T3D

def assertAlmostEqual (a, b):
   assert abs (a - b) <= 0.00000001

def assertAlmostEqualList (a, b):
   for _a, _b in zip (a, b):
      assertAlmostEqual (_a, _b)

class TestTortoise3D:

   @pytest.fixture ()
   def t (self):
      return T3D ()

   def testConstructor (self, t):
      assert isinstance (t, T3D)

   def testGetItem (self, t):

      # Simple properties:
      assert t ['radians' ] == True
      assert t ['distance'] == 1

      # Stack, only test for length:
      assert len (t ['stack']) == 0

      # Default position:
      (x, y, z) = (0, 0, 0)
      assert t ['position' ] == (x, y, z)
      assert t ['positionX'] == x
      assert t ['positionY'] == y
      assert t ['positionZ'] == z

      # Default local axes:
      (X, Y, Z) = ((1, 0, 0), (0, 1, 0), (0, 0, 1))
      assert t ['local' ] == (X, Y, Z)
      assert t ['localX'] == X
      assert t ['localY'] == Y
      assert t ['localZ'] == Z

   def testIAdd (self, t):

      # Move to (1, 2, 3):
      t += (1, 2, 3)
      assert t ['position'] == (1, 2, 3)

      # += operates in world space, rotation should not affect it:
      t.rotateY (math.pi / 2)
      t += (1, 2, 3)
      assert t ['position'] == (2, 4, 6)

   def testISub (self, t):

      # Move to (-1, -2, -3):
      t -= (1, 2, 3)
      assert t ['position'] == (-1, -2, -3)

      # -= operates in world space, rotation should not affect it:
      t.rotateZ (math.pi / 2)
      t -= (1, 2, 3)
      assert t ['position'] == (-2, -4, -6)

   def testAbs (self, t):

      assert abs (t) == 0

      t += (1, 1, 1)
      assertAlmostEqual (abs (t), math.sqrt (3))

      t += (1, 2, 3)
      assertAlmostEqual (abs (t), math.sqrt (4 + 9 + 16))

   def testRotateRadians (self, t):

      # Rotate by 360° around an arbitrary axis:
      t.rotate (2 * math.pi, (1, 2, 3))
      assertAlmostEqualList (t ['localX'], (1, 0, 0))
      assertAlmostEqualList (t ['localY'], (0, 1, 0))
      assertAlmostEqualList (t ['localZ'], (0, 0, 1))

      # Rotate by 180° around the y-axis (world space):
      t.rotate (math.pi, (0, 1, 0))
      assertAlmostEqualList (t ['localX'], (-1, 0,  0))
      assertAlmostEqualList (t ['localY'], ( 0, 1,  0))
      assertAlmostEqualList (t ['localZ'], ( 0, 0, -1))

      # Rotate by 90° around the z-axis (world space):
      t.rotate (math.pi / 2, (0, 0, 1))
      assertAlmostEqualList (t ['localX'], ( 0, -1,  0))
      assertAlmostEqualList (t ['localY'], (-1,  0,  0))
      assertAlmostEqualList (t ['localZ'], ( 0,  0, -1))

   def testRotateDegrees (self, t):

      t.radians (False)

      # Rotate by 360° around an arbitrary axis:
      t.rotate (360, (1, 2, 3))
      assertAlmostEqualList (t ['localX'], (1, 0, 0))
      assertAlmostEqualList (t ['localY'], (0, 1, 0))
      assertAlmostEqualList (t ['localZ'], (0, 0, 1))

      # Rotate by 180° around the y-axis (world space):
      t.rotate (180, (0, 1, 0))
      assertAlmostEqualList (t ['localX'], (-1, 0,  0))
      assertAlmostEqualList (t ['localY'], ( 0, 1,  0))
      assertAlmostEqualList (t ['localZ'], ( 0, 0, -1))

      # Rotate by 90° around the z-axis (world space):
      t.rotate (90, (0, 0, 1))
      assertAlmostEqualList (t ['localX'], ( 0, -1,  0))
      assertAlmostEqualList (t ['localY'], (-1,  0,  0))
      assertAlmostEqualList (t ['localZ'], ( 0,  0, -1))

   def testRotateXYZ (self, t):

      # Rotate by 90° around the local x-axis:
      t.rotateX (math.pi / 2)
      assertAlmostEqualList (t ['localX'], (1,  0, 0))
      assertAlmostEqualList (t ['localY'], (0,  0, 1))
      assertAlmostEqualList (t ['localZ'], (0, -1, 0))

      # Rotate by 180° around the local y-axis:
      t.rotateY (math.pi)
      assertAlmostEqualList (t ['localX'], (-1, 0, 0))
      assertAlmostEqualList (t ['localY'], ( 0, 0, 1))
      assertAlmostEqualList (t ['localZ'], ( 0, 1, 0))

      # Rotate by 90° around the local z-axis:
      t.radians (False)
      t.rotateZ (90)
      assertAlmostEqualList (t ['localX'], (0, 0, 1))
      assertAlmostEqualList (t ['localY'], (1, 0, 0))
      assertAlmostEqualList (t ['localZ'], (0, 1, 0))

   def testRotateWorldXYZ (self, t):

      # Rotate by 180° around the world z-axis:
      t.rotateWorldZ (math.pi)
      assertAlmostEqualList (t ['localX'], (-1,  0, 0))
      assertAlmostEqualList (t ['localY'], ( 0, -1, 0))
      assertAlmostEqualList (t ['localZ'], ( 0,  0, 1))

      # Rotate by 90° around the world y-axis:
      t.radians (False)
      t.rotateWorldY (90)
      assertAlmostEqualList (t ['localX'], (0,  0, 1))
      assertAlmostEqualList (t ['localY'], (0, -1, 0))
      assertAlmostEqualList (t ['localZ'], (1,  0, 0))

      # Rotate by 180° around the world x-axis:
      t.rotateWorldX (180)
      assertAlmostEqualList (t ['localX'], (0, 0, -1))
      assertAlmostEqualList (t ['localY'], (0, 1,  0))
      assertAlmostEqualList (t ['localZ'], (1, 0,  0))

   def testMove (self, t):

      # Move by two units in direction (1, 1, 1). move() does not modify
      # the provided vector, we have to create a unit vector manually:
      l = 1 / math.sqrt (3)
      t.distance (2)
      t.move ((l, l, l))
      assertAlmostEqualList (t ['position'], (2 * l, 2 * l, 2 * l))

      # The direction is specified in world space, rotation should not
      # affect move():
      t.rotateX (math.pi / 2)
      t.rotateY (math.pi / 2)
      t.rotateZ (math.pi / 2)
      t.distance (1)
      t.move ((-l, -l, -l))
      assertAlmostEqualList (t ['position'], (l, l, l))

      # Manually specifying the distance:
      t.move ((-l, -l, -l), 2)
      assertAlmostEqualList (t ['position'], (-l, -l, -l))

   def testForwardXYZ (self, t):

      # Operates on local space:
      t.rotateX (math.pi / 2)
      t.rotateY (math.pi / 2)
      t.rotateZ (math.pi / 2)

      # Simple forward:
      t.forwardX ()
      assertAlmostEqualList (t ['position'], (0, 0, 1))

      # Setting the default distance:
      t.distance (2)
      t.forwardY ()
      assertAlmostEqualList (t ['position'], (0, -2, 1))

      # Explicitely specifying a distance:
      t.forwardZ (3)
      assertAlmostEqualList (t ['position'], (3, -2, 1))

   def testBackward (self, t):

      # Operates on local space:
      t.rotateX (math.pi / 2)
      t.rotateY (math.pi / 2)
      t.rotateZ (math.pi / 2)

      # Simple backward:
      t.backward ()
      assertAlmostEqualList (t ['position'], (0, 0, -1))

      # Setting the default distance:
      t.distance (2)
      t.backward ()
      assertAlmostEqualList (t ['position'], (0, 0, -3))

      # Explicitely specifying a distance:
      t.backward (3)
      assertAlmostEqualList (t ['position'], (0, 0, -6))

   def testGoto (self, t):

      # Rotation should not affect goto():
      for i in range (10):
         t.rotateX (random.uniform (0, 2 * math.pi))
         t.rotateY (random.uniform (0, 2 * math.pi))
         t.rotateZ (random.uniform (0, 2 * math.pi))
         for x in range (10):
            for y in range (10):
               for z in range (10):
                     t.goto ((x, y, z))
                     assertAlmostEqualList (t ['position'], (x, y, z))

   def testDistance (self, t):

      assert t ['distance'] == 1
      assert t.distance ()  == 1

      old = 1
      for i in range (10):
         d = random.uniform (0, 20)
         assert t.distance (d) == old
         assert t ['distance'] == d
         assert t.distance ()  == d
         old = d

   def testRadians (self, t):

      assert t ['radians'] == True
      assert t.radians ()  == True

      assert t.radians (False) == True
      assert t ['radians']     == False
      assert t.radians ()      == False

      assert t.radians (True) == False
      assert t ['radians']    == True
      assert t.radians ()     == True

   def testPushPop (self, t):

      t.push ()
      assert len (t ['stack']) == 1

      t.rotate   (math.pi / 4, (1, 1, 1))
      t.forwardX (1)
      t.forwardY (2)
      t.forwardZ (3)
      t.radians  (False)
      t.distance (2)

      t.pop ()
      assert len (t ['stack']) == 0

      assert t ['localX'  ] == (1, 0, 0)
      assert t ['localY'  ] == (0, 1, 0)
      assert t ['localZ'  ] == (0, 0, 1)
      assert t ['position'] == (0, 0, 0)
      assert t ['distance'] == 1
      assert t ['radians' ] == True

      with pytest.raises (IndexError):
         t.pop ()

   def testAliases (self, t):

      assert t.rotateX  == t.roll
      assert t.rotateY  == t.yaw
      assert t.rotateZ  == t.pitch
      assert t.forwardX == t.forward

# :indentSize=3:tabSize=3:noTabs=true:mode=python:maxLineLen=72: