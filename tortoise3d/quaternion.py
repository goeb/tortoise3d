# This file is part of the Tortoise3D package for Python.
#
# Tortoise3D is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Tortoise3D is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Tortoise3D. If not,
# see <http://www.gnu.org/licenses/>.
#
# Copyright 2008-2010, 2014 Stefan Goebel <tortoise3d /at\ subtype \dot/ de>

from __future__ import division
from __future__ import print_function

import math

class Quaternion:

    """Quaternion class.

    Most required quaternion operations are available, as well as methods for rotating and
    converting the quaternion to a matrix.

    A quaternion is defined as: q = w + x*i + y*j + z*k, with i, j and k being imaginary factors,
    w, x, y and z are real. In this class, only w, x, y and z will be used, for example, when a
    method returns an imaginary part only the real coefficient (either x, y or z) is returned.

    See <http://en.wikipedia.org/wiki/Quaternion> for more information on quaternions.

    Example usage:

        Rotate the point (1, 2, 3) around the axis (0, 1, 0) with an angle of 45 degrees, and then
        around the axis (1, 0, 0) with an angle of 60 degrees:

        >>> import tortoise3d
        >>> import math
        >>> originalPoint = tortoise3d.Quaternion (0, 1, 2, 3)
        >>> rotation1 = tortoise3d.Quaternion (math.radians (45), (0, 1, 0))
        >>> rotation2 = tortoise3d.Quaternion (math.radians (60), (1, 0, 0))
        >>> rotatedPoint = originalPoint.rotate ([rotation1, rotation2])

    Note that this class currently uses floating point numbers, values may not be exact, especially
    after a number of operations!

    WARNING: Most of the code is untested! Please file a bug report if you encounter any errors.
    """

    __version__ = '0.3'

    def __init__ (self, *data):

        """Constructor.

        If called with two parameters, it is assumed that the quaternion will be used as a rotation
        quaternion. The first parameter should be an angle in radians, the second parameter should
        be a list with three elements, representing the axis vector for the rotation. The actual
        values for the quaternion will be calculated based on these parameters.

        If called with four parameters, the quaternion will be initialized with these values without
        any further processing.
        """

        if len (data) == 4:
            self.__w = float (data [0])
            self.__x = float (data [1])
            self.__y = float (data [2])
            self.__z = float (data [3])

        elif len (data) == 2:
            self.__w = math.cos (float (data [0]) / 2)
            self.__x = data [1] [0] * math.sin (float (data [0]) / 2)
            self.__y = data [1] [1] * math.sin (float (data [0]) / 2)
            self.__z = data [1] [2] * math.sin (float (data [0]) / 2)

    def __getitem__ (self, key):

        """Indexed access to read the quaternion elements.

        Access to the elements of the quaternion is possible with the keys 0-3 and 'w'-'z', where
        quaternion [0] is the same as quaternion ['w'] (the real part), quaternion [1] is the same
        as quaternion ['x'] (the first row of the imaginary vector), etc. The elements can also be
        accessed using the w(), x(), y() and z() methods.

        Returns the requested element of the quaternion.
        """

        if   key == 0 or key == 'w':
            return self.__w
        elif key == 1 or key == 'x':
            return self.__x
        elif key == 2 or key == 'y':
            return self.__y
        elif key == 3 or key == 'z':
            return self.__z
        else:
            raise IndexError

    def __setitem__ (self, key, value):

        """Indexed access to change the quaternion elements.

        Access to the elements of the quaternion is possible with the keys 0-3 and 'w'-'z', where
        quaternion [0] is the same as quaternion ['w'] (the real part), quaternion [1] is the
        same as quaternion ['x'] (the first row of the imaginary vector), etc. The elements can
        also be changed using the w(), x(), y() and z() methods.
        """

        if   key == 0 or key == 'w':
            self.__w = value
        elif key == 1 or key == 'x':
            self.__x = value
        elif key == 2 or key == 'y':
            self.__y = value
        elif key == 3 or key == 'z':
            self.__z = value
        else:
            raise IndexError

    def __mul__ (self, q):

        """Multiplication operator.

        If the right hand side is not indexable, it is assumed to be a scalar value (int, float,
        etc.) and every element of the quaternion is multiplied by the value. If the right hand side
        is indexable, it is assumed to be a quaternion (or a list or list like object with four
        elements) and the two quaternions are multiplied (Hamilton product). This operation is not
        commutative.

        This method returns the resulting quaternion (a new Quaternion instance).
        """

        try:
            q [0]

        except TypeError:
            w = self.__w * q
            x = self.__x * q
            y = self.__y * q
            z = self.__z * q

        else:
            w = self.__w * q [0] - self.__x * q [1] - self.__y * q [2] - self.__z * q [3]
            x = self.__w * q [1] + self.__x * q [0] + self.__y * q [3] - self.__z * q [2]
            y = self.__w * q [2] - self.__x * q [3] + self.__y * q [0] + self.__z * q [1]
            z = self.__w * q [3] + self.__x * q [2] - self.__y * q [1] + self.__z * q [0]

        return Quaternion (w, x, y, z)

    def __div__ (self, q):

        """Division operator.

        The right hand side has to be a scalar number (real). All elements of the quaternion will
        be divided by it. __truediv__ is the same as __div__ for Python 3 compatibility.

        This method returns the resulting quaternion (a new Quaternion instance).
        """

        return Quaternion (self.__w / q, self.__x / q, self.__y / q, self.__z / q)

    __truediv__ = __div__

    def __add__ (self, q):

        """Addition operator.

        Right hand side has to be a quaternion (or a list or list like type with four elements),
        addition is done element by element.

        This method returns the resulting quaternion (a new Quaternion instance).
        """

        w = self.__w + q [0]
        x = self.__x + q [1]
        y = self.__y + q [2]
        z = self.__z + q [3]

        return Quaternion (w, x, y, z)

    def __sub__ (self, q):

        """Subtraction operator.

        Right hand side has to be a quaternion (or a list or list like type with four elements),
        subtraction is done element by element.

        This method returns the resulting quaternion (a new Quaternion instance).
        """

        w = self.__w - q [0]
        x = self.__x - q [1]
        y = self.__y - q [2]
        z = self.__z - q [3]

        return Quaternion (w, x, y, z)

    def __imul__ (self, q):

        """In-place multiplication operator.

        Same as __mul__(), except that the quaternion is modified instead of returning the result.
        """

        try:
            q [0]

        except TypeError:
            w = self.__w * q
            x = self.__x * q
            y = self.__y * q
            z = self.__z * q

        else:
            w = self.__w * q [0] - self.__x * q [1] - self.__y * q [2] - self.__z * q [3]
            x = self.__w * q [1] + self.__x * q [0] + self.__y * q [3] - self.__z * q [2]
            y = self.__w * q [2] - self.__x * q [3] + self.__y * q [0] + self.__z * q [1]
            z = self.__w * q [3] + self.__x * q [2] - self.__y * q [1] + self.__z * q [0]

        self.__w = w
        self.__x = x
        self.__y = y
        self.__z = z

        return self

    def __idiv__ (self, q):

        """In-place division operator.

        Same as __div__(), except that the quaternion is modified instead of returning the result.
        """

        self.__w = self.__w / q
        self.__x = self.__x / q
        self.__y = self.__y / q
        self.__z = self.__z / q

        return self

    __itruediv__ = __idiv__

    def __iadd__ (self, q):

        """In-place addition operator.

        Same as __add__(), except that the quaternion is modified instead of returning the result.
        """

        self.__w = self.__w + q [0]
        self.__x = self.__x + q [1]
        self.__y = self.__y + q [2]
        self.__z = self.__z + q [3]

        return self

    def __isub__ (self, q):

        """In-place subtraction operator.

        Same as __sub__(), except that the quaternion is modified instead of returning the result.
        """

        self.__w = self.__w - q [0]
        self.__x = self.__x - q [1]
        self.__y = self.__y - q [2]
        self.__z = self.__z - q [3]

        return self

    def __abs__ (self):

        """Absolute value of the quaternion.

        This method returns a scalar number, the absolute value (length) of the quaternion.
        """

        return math.sqrt (self.__w ** 2 + self.__x ** 2 + self.__y ** 2 + self.__z ** 2)

    def __eq__ (self, q):

        """Equality operator.

        This method returns a boolean value, True if the four elements of both quaternions (or
        lists or list like types) are equal, else False. Note that - since this class uses floating
        point numbers - the result may not be as expected.
        """

        return                    \
            self.__w == q [0] and \
            self.__x == q [1] and \
            self.__y == q [2] and \
            self.__z == q [3]

    def __ne__ (self, q):

        """Non-equality operator.

        Same as not __eq__(), see __eq__() for details.
        """

        return not self.__eq__ (q)

    def __repr__ (self):

        """Internal representation of the quaternion (more or less).

        The representation will look like a list containing the four elements of the quaternion.
        """

        return '[%f, %f, %f, %f]' % (self.__w, self.__x, self.__y, self.__z)

    def __len__ (self):

        """Length of the quaternion.

        Note that this is the length of the list representing the quaternion (which is always 4). To
        get the absolute value, use abs() (see the __abs__() method).

        This method always returns 4.
        """

        return 4

    def w (self, w = None):

        """Get or set the real part.

        If called without argument this method returns the current real part of the quaternion. If
        an argument is given, the real part will be set to this value and this method returns the
        old value. The real() method is an alias for w().
        """

        if w is None:
            return self.__w
        else:
            old = self.__w
            self.__w = w
            return old

    real = w

    def x (self, x = None):

        """Get or set the first imaginary part.

        If called without argument this method returns the current value of the first imaginary part
        of the quaternion. If an argument is given, the first imaginary part will be set to this
        value and this method returns the old value.
        """

        if x is None:
            return self.__x
        else:
            old = self.__x
            self.__x = x
            return old

    def y (self, y = None):

        """Get or set the second imaginary part.

        If called without argument this method returns the current value of the second imaginary
        part of the quaternion. If an argument is given, the second imaginary part will be set to
        this value and this method returns the old value.
        """

        if y is None:
            return self.__y
        else:
            old = self.__y
            self.__y = y
            return old

    def z (self, z = None):

        """Get or set the third imaginary part.

        If called without argument this method returns the current value of the third imaginary part
        of the quaternion. If an argument is given, the third imaginary part will be set to this
        value and this method returns the old value.
        """

        if z is None:
            return self.__z
        else:
            old = self.__z
            self.__z = z
            return old

    def vector (self, v = None):

        """Get or set the imaginary part.

        If called without argument this method returns the current values of the imaginary parts of
        the quaternion as a tuple. If an argument is given, it has to be a tuple (or another list
        like object) containing three elements, the imaginary parts will be set to this values and
        this method returns the old values as a tuple.
        """

        if v is None:
            return (self.__x, self.__y, self.__z)
        elif len (v) == 3:
            old = (self.__x, self.__y, self.__z)
            self.__x = v [0]
            self.__y = v [1]
            self.__z = v [2]
            return old
        else:
            raise IndexError

    def argument (self):

        """Argument of the quaternion.

        This method returns the argument, i.e. the angle of rotation around the rotation axis, as a
        scalar number in radians.
        """

        return math.acos (self.__w / abs (self))

    def versor (self):

        """Versor of the quaternion.

        This method returns a new Quaternion instance representing the current one, but with an
        absolute value of 1, a unit quaternion.
        """

        return self / abs (self)

    def conjugate (self):

        """Conjugation of the quaternion.

        This method returns the conjugated quaternion (a new Quaternion instance).
        """

        return Quaternion (self.__w, -1 * self.__x, -1 * self.__y, -1 * self.__z)

    def inverse (self):

        """Inverse (reciprocal) of the quaternion.

        This method returns the inverse (or reciprocal) of the quaternion (a new Quaternion
        instance).
        """

        return self.conjugate () / (self.__w ** 2 + self.__x ** 2 + self.__y ** 2 + self.__z ** 2)

    def negate (self):

        """Negated quaternion.

        This method returns the negated quaternion (a new Quaternion instance).
        """

        return self * -1

    def innerProduct (self, q):

        """Inner product (dot-product).

        The argument to this method should be a quaternion (or any other list or list like type with
        four elements).

        This method returns the inner product of the two quaternions, a scalar number.
        """

        return self.__w * q [0] + self.__x * q [1] + self.__y * q [2] + self.__z * q [3]

    def outerProduct (self, q):

        """Outer product.

        The argument to this method should be a quaternion (or any other list or list like type with
        four elements).

        This method returns the outer product (a new Quaternion instance) of the two quaternions.
        """

        return (self.conjugate () * q - self * q.conjugate ()) / 2

    def crossProduct (self, q):

        """Cross product.

        The argument to this method should be a quaternion (or any other list or list like type with
        four elements).

        This method returns the cross product of the two quaternions. The cross product contains
        only imaginary parts, the return type is a tuple with three elements.
        """

        return (
            self.__y * q [3] - self.__z * q [2],
            self.__z * q [1] - self.__x * q [3],
            self.__x * q [2] - self.__y * q [1],
        )

    def evenProduct (self, q):

        """Even product (Hamilton inner product).

        The argument to this method should be a quaternion (or any other list or list like type with
        four elements).

        This method returns the even product (a new Quaternion instance) of the two quaternions.
        """

        return (self * q - q * self) / 2

    def euclidianProduct (self, q):

        """Euclidian product.

        The argument to this method should be a quaternion (or any other list or list like type with
        four elements).

        This method returns the euclidian product (a new Quaternion instance) of the two
        quaternions.
        """

        return self.conjugate () * q

    def rotate (self, qs):

        """Apply a rotation to the quaternion.

        The current quaternion represents a point in space (or a vector). The real part must be set
        to zero, the imaginary parts must be set to the x, y, z values of the point that should be
        rotated. The argument to this method must be a list of quaternions used to rotate the point
        (see __init__() and the example usage for details on how to create these). Note that the
        list is applied starting with the last element, this is most likely exactly what you want.
        The rotation quaternions should have an absolute value of 1, use the versor() method if this
        is not the case.

        Returns a new Quaternion instance, the imaginary parts of this quaternion represent the
        rotated point (or vector).
        """

        rotation = qs.pop ()
        while len (qs) > 0:
            rotation = rotation * qs.pop ()
        return rotation * self * rotation.conjugate ()

    def roundAll (self, n = 10):

        """Round all elements of the quaternion.

        Since floating point numbers are used by this class, accuracy will decrease with every
        operation. This method rounds all elements of the quaternion, to get values like 1.23e-18
        back to a reasonable number (in this case 0.0). The elements will be rounded to ten digits
        after the decimal point by default, an argument may be specified to change the number of
        digits.

        This method returns the resulting quaternion (a new Quaternion instance).
        """

        return Quaternion (
            round (self.__w, n),
            round (self.__x, n),
            round (self.__y, n),
            round (self.__z, n)
        )

    def complexMatrix (self):

        """Complex 2x2 matrix representing the quaternion.

        This method returns a 2x2 matrix containing complex numbers, that represents the quaternion.
        The matrix is returned as a list of lists, in the form [[row 1], [row 2]].
        """

        return [
            [     self.__w + self.__x * 1j, self.__y + self.__z * 1j],
            [-1 * self.__y + self.__z * 1j, self.__w - self.__x * 1j],
        ]

    def realMatrix (self):

        """Real 4x4 matrix representing the quaternion.

        This method returns a 4x4 matrix containing real numbers, that represents the quaternion.
        The matrix is returned as a list of lists, in the form [[row 1], [row 2], [row 3], [row 4]].
        """

        return [
            [     self.__w,      self.__x,      self.__y,      self.__z],
            [-1 * self.__x,      self.__w, -1 * self.__z,      self.__y],
            [-1 * self.__y,      self.__z,      self.__w, -1 * self.__x],
            [-1 * self.__z, -1 * self.__y,      self.__x,      self.__w]
        ]

    def rotationMatrix (self):

        """3x3 rotation matrix representing the quaternion rotation.

        This method returns a 3x3 matrix containing real numbers, that represents the rotation that
        would be applied by the quaternion. You may use it like any other rotation matrix. The
        matrix is returned as a list of lists, in the form [[row 1], [row 2], [row 3]].
        """
        w2 = self.__w ** 2
        x2 = self.__x ** 2
        y2 = self.__y ** 2
        z2 = self.__z ** 2

        wx = self.__w * self.__x
        wy = self.__w * self.__y
        wz = self.__w * self.__z
        xy = self.__x * self.__y
        xz = self.__x * self.__z
        yz = self.__y * self.__z

        return [
            [ w2 + x2 - y2 - z2,    2 * xy - 2 * wz,      2 * xz + 2 * wy   ],
            [ 2 * xy + 2 * wz,      w2 - x2 + y2 - z2,    2 * yz - 2 * wx   ],
            [ 2 * xz - 2 * wy,      2 * yz - 2 * wx,      w2 - x2 - y2 + z2 ],
        ]

if __name__ == '__main__':

    print ('\n'.join ([
         'Quaternion module - Version 0.3 - 2014-06-03'
        ,''
        ,'Copyright 2008, 2014 Stefan Goebel <tortoise3d |at| subtype |dot| de>'
        ,''
        ,'This file is part of the Tortoise3D package for Python.'
        ,'' 
        ,'Tortoise3D is free software: you can redistribute it and/or modify it under the terms'
        ,'of the GNU General Public License as published by the Free Software Foundation, either'
        ,'version 3 of the License, or (at your option) any later version.'
        ,''
        ,'Tortoise3D is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;'
        ,'without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.'
        ,'See the GNU General Public License for more details.'
        ,''
        ,'You should have received a copy of the GNU General Public License along with Tortoise3D.'
        ,'If not, see <http://www.gnu.org/licenses/>.'
    ]))

# :indentSize=4:tabSize=4:noTabs=true:mode=python:maxLineLen=100: