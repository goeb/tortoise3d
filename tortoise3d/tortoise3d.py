# This file is part of the Tortoise3D package for Python.
#
# Tortoise3D is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Tortoise3D is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Tortoise3D. If not,
# see <http://www.gnu.org/licenses/>.
#
# Copyright 2008-2010, 2014 Stefan Goebel <tortoise3d /at\ subtype \dot/ de>

from __future__ import print_function
from __future__ import absolute_import

import math
from . import quaternion

class Tortoise3D:

    """Tortoise3D - Like 3D turtle graphics, but without the graphics part.

    This class does not provide any graphical output, it allows you to move a turtle (tortoise) in
    3D space and query the coordinates to include it in your own scripts or modules.

    A Tortoise3D instance keeps track of its position in world space and a local coordinate system.
    This local space may differ from the world coordinate system as a result of a rotation of the
    tortoise. The basic movement operations - e.g. forwardX() - move the tortoise by a user defined
    distance (by default this distance is 1). These basic movements always operate on the local
    coordinate system. A tortoise instance also maintains a simple stack, allowing you to push and
    pop the tortoise's state.

    Example usage:

        Create the Tortoise3D instance, move it one unit in x direction, rotate it by 90 degrees
        around the y-axis and move it one unit in the new x direction. Save the tortoise position in
        the variables x, y and z.

        >>> import tortoise3d
        >>> tortoise = tortoise3d.Tortoise3D ()
        >>> tortoise.forwardX ()
        ((0.0, 0.0, 0.0), (1.0, 0.0, 0.0))
        >>> tortoise.radians (False)
        True
        >>> tortoise.rotateY (90)
        >>> (_, (x, y, z)) = tortoise.forwardX ()

    Note that this class uses the quaternion module, which uses floating point numbers for its
    calculations. Values may not be as precise as expected, especially after a number of operations!
    """

    __version__ = '0.3'

    def __init__ (self):

        """Constructor.

        Initializes a new tortoise instance. The initial position will be (0, 0, 0). The initial
        local coordinate system of the tortoise equals the world coordinate sytem (i.e. there is no
        initial rotation). The distance will be set to 1 by default. The default unit for specifying
        angles will be radians. The stack will be initially empty.
        """

        self.reset ()

    def reset (self):

        """Reset the tortoise to the initial default values.

        This method will reset the state of the tortoise instance to the initial state. See the
        constructor __init__() for details on the individual values.
        """

        self.__position = quaternion.Quaternion (0, 0, 0, 0)
        self.__distance = 1
        self.__stack    = []
        self.__radians  = True
        self.__local    = (
            quaternion.Quaternion (0, 1, 0, 0),
            quaternion.Quaternion (0, 0, 1, 0),
            quaternion.Quaternion (0, 0, 0, 1)
        )

    def __repr__ (self):

        """Return a string representation of the state of the tortoise instance.

        This function returns the representation string of the tortoise instance. Note that for
        floating point numbers the string representation will be used instead of including these
        themselves. This is really only useful for debugging.
        """

        return (
            '{'                  '\n'
            '    "position": %s' '\n'
            '    "local":    ['  '\n'
            '                %s' '\n'
            '                %s' '\n'
            '                %s' '\n'
            '    ],'             '\n'
            '    "distance": %s' '\n'
            '    "radians":  %s' '\n'
            '    "stack":    %s' '\n'
            '}'
            %
            (
                self.__position,
                self.__local [0],
                self.__local [1],
                self.__local [2],
                self.__distance,
                self.__radians,
                self.__stack
            )
        )

    def __getitem__ (self, key):

        """Indexed access to read the tortoise properties.

        The following keys are defined (strings, unquoted here for brevity):

            local, localX, localY, localZ             - local coordinate system (and x-, y-, z-axis)
            position, positionX, positionY, positionZ - current position
            distance                                  - distance for movement operations
            radians                                   - angle unit (True = radians, False = degrees)
            stack                                     - the tortoise's stack

        'local' will return a tuple containing the three axes of the tortoise's local coordinate
        system, 'localX' etc. will only return the requested axis. All returned axes are tuples with
        three elements. The other values should be self-explanatory.

        Returns the requested property of the tortoise instance.
        """

        return {

            'local'     : (
                self.__local [0].vector (),
                self.__local [1].vector (),
                self.__local [2].vector ()
            ),

            'localX'    : self.__local [0].vector (),
            'localY'    : self.__local [1].vector (),
            'localZ'    : self.__local [2].vector (),

            'position'  : self.__position.vector (),
            'positionX' : self.__position [1],
            'positionY' : self.__position [2],
            'positionZ' : self.__position [3],

            'distance'  : self.__distance,
            'radians'   : self.__radians,
            'stack'     : self.__stack,

        } [key]

    def __iadd__ (self, q):

        """In-place addition operator to change the tortoise position.

        You may use the += operator on a tortoise instance to change its position. The new position
        will be calculated based on the world coordinate system. The argument must be a list or list
        like type with three elements, specifying the position change in x, y and z direction.
        """

        self.__position += (0, q [0], q [1], q [2])

        return self

    def __isub__ (self, q):

        """In-place subtraction operator to change the tortoise position.

        You may use the -= operator on a tortoise instance to change its position. The new position
        will be calculated based on the world coordinate system. The argument must be a list or list
        like type with three elements, specifying the position change in negative x, y and z
        direction.
        """

        self.__position -= (0, q [0], q [1], q [2])

        return self

    def __abs__ (self):

        """Return the distance of the tortoise from the point of origin.

        This method returns the distance of the tortoise from the point of origin. The return value
        will be a floating point number.
        """

        return abs (self.__position)

    def rotate (self, a, v):

        """Rotate the tortoise around an arbitrary axis.

        The first parameter specifies the angle, either in radians (default) or in degrees (you
        may change this with the radians() method). The second argument specifies the rotation axis
        (in world space). This must be a list or list like object with three elements.
        """

        if not self.__radians:
            a = math.radians (a)

        rotation = quaternion.Quaternion (a, (v [0], v [1], v [2])).versor ()

        self.__local = (
            self.__local [0].rotate ([rotation]).versor (),
            self.__local [1].rotate ([rotation]).versor (),
            self.__local [2].rotate ([rotation]).versor (),
        )

    def rotateX (self, a):

        """Rotate the tortoise around the local x-axis.

        The parameter specifies the angle, either in radians (default) or in degrees (you may change
        this with the radians() method). roll() is an alias for this method.
        """

        return self.rotate (a, self.__local [0].vector ())

    roll = rotateX

    def rotateY (self, a):

        """Rotate the tortoise around the local y-axis.

        The parameter specifies the angle, either in radians (default) or in degrees (you may change
        this with the radians() method). yaw() is an alias for this method.
        """

        return self.rotate (a, self.__local [1].vector ())

    yaw = rotateY

    def rotateZ (self, a):

        """Rotate the tortoise around the local z-axis.

        The parameter specifies the angle, either in radians (default) or in degrees (you may change
        this with the radians() method). pitch() is an alias for this method.
        """

        return self.rotate (a, self.__local [2].vector ())

    pitch = rotateZ

    def rotateWorldX (self, a):

        """Rotate the tortoise around the world x-axis (i.e. (1, 0, 0)).

        The parameter specifies the angle, either in radians (default) or in degrees (you may change
        this with the radians() method).
        """

        return self.rotate (a, (1, 0, 0))

    def rotateWorldY (self, a):

        """Rotate the tortoise around the world y-axis (i.e. (0, 1, 0)).

        The parameter specifies the angle, either in radians (default) or in degrees (you may change
        this with the radians() method).
        """

        return self.rotate (a, (0, 1, 0))

    def rotateWorldZ (self, a):

        """Rotate the tortoise around the world z-axis (i.e. (0, 0, 1)).

        The parameter specifies the angle, either in radians (default) or in degrees (you may change
        this with the radians() method).
        """

        return self.rotate (a, (0, 0, 1))

    def move (self, v, d = None):

        """Move the tortoise along an arbitrary direction.

        The first argument should be a list with three elements that specifies the direction vector
        for the movement, the second (optional) argument specifies the distance (it defaults to the
        current distance setting). Note that the direction vector is used without any modification,
        i.e. if you want a unit vector you have to do this yourself!

        This function returns a tuple of two tuples, each containing three values. The first
        tuple specifies the old position, the second the new position of the tortoise (world space).
        """

        if d is None:
            d = self.__distance

        old = self.__position.vector ()
        self.__position += (0, v [0] * d, v [1] * d, v [2] * d)

        return (old, self.__position.vector ())

    def forwardX (self, d = None):

        """Move the tortoise along its local x-axis.

        The (optional) argument specifies the distance (defaults to the current distance setting).

        This method returns a tuple of two tuples, each containing three values. The first tuple
        specifies the old position, the second the new position of the tortoise. The forward()
        method is an alias for forwardX().
        """

        return self.move (self.__local [0].vector (), d)

    forward = forwardX

    def forwardY (self, d = None):

        """Move the tortoise along its local y-axis.

        The (optional) argument specifies the distance (defaults to the current distance setting).

        This method returns a tuple of two tuples, each containing three values. The first tuple
        specifies the old position, the second the new position of the tortoise.
        """

        return self.move (self.__local [1].vector (), d)

    def forwardZ (self, d = None):

        """Move the tortoise along its local z-axis.

        The (optional) argument specifies the distance (defaults to the current distance setting).

        This method returns a tuple of two tuples, each containing three values. The first tuple
        specifies the old position, the second the new position of the tortoise.
        """

        return self.move (self.__local [2].vector (), d)

    def backward (self, d = None):

        """Move the tortoise backwards along its local x-axis.

        Same as forward(), but in the opposite direction. See forward() for details.
        """

        if d is None:
            d = self.__distance

        return self.forwardX (-1 * d)

    def goto (self, pos):

        """Set the position of the tortoise.

        The new position must be specified as a tuple containing three values (x, y, z). These
        coordinates are in world space.

        This function returns a tuple of two tuples, each containing three values. The first
        tuple specifies the old position, the second the new position of the tortoise.
        """

        old = self.__position.vector ()
        self.__position = quaternion.Quaternion (0, pos [0], pos [1], pos [2])

        return old

    def distance (self, d = None):

        """Get or set the distance for the movement functions.

        If called without argument this function returns the current distance. If an argument is
        given, the distance will be set to this value and this method returns the old value.
        """

        if d is None:
            return self.__distance
        else:
            old = self.__distance
            self.__distance = d
            return old

    def radians (self, r = None):

        """Get or set the radians setting.

        If called without argument this function returns the current radians setting. If an argument
        is given, the setting will be set to this value and this method returns the old value.

        If radians is True, all angles must be specified in radians. If it is False, all angles must
        be specified in degrees.
        """

        if r is None:
            return self.__radians
        else:
            old = self.__radians
            self.__radians = r
            return old

    def push (self):

        """Saves the current state of the tortoise on the stack.

        The current state is appended to the tortoise's stack and may be restored using the pop()
        function. Note that properties like the radians and distance settings will also be stored.

        This function returns the current state of the tortoise, without the stack.
        """

        self.__stack.append (self.state ())

        return self.__stack [-1]

    def pop (self):

        """Restores the previous pushed state of the tortoise.

        The tortoise's state is set to the last state on the stack, this state is removed from the
        stack. This function will raise an error if the stack is empty. Note that properties like
        the radians and distance settings will also be restored.

        This function returns the current state of the tortoise (i.e. the state before restoring the
        previous state) without the stack.
        """

        old   = self.state ()
        stack = self.__stack

        self.state (stack.pop ())
        self.__stack = stack

        return old

    def state (self, state = None, stack = False):

        """Get or set the state of the turtle.

        If called without any argument this function returns the current state of the turtle without
        the stack.

        If an argument is given (first argument, or 'state'), the state will be set to this value
        and this function returns the old value. Note that the argument must be a valid state
        structure, as returned by this function.

        The named argument 'stack' may be set to True if the returned state should include the stack
        of the tortoise, it is set to False by default, meaning the stack will not be included. This
        argument does not affect the behaviour when a state is restored: If the stored state has a
        stack, it will always be restored. Else the stack will be empty after restoring the state.
        """

        # pylint: disable=W0201

        old = {
            'local'    : (
                self.__local [0].vector (),
                self.__local [1].vector (),
                self.__local [2].vector ()
            ),
            'position' : self.__position.vector (),
            'distance' : self.__distance,
            'radians'  : self.__radians,
        }

        if stack:
            old ['stack'] = self.__stack

        if state is None:
            return old

        del self.__local
        self.__local = (
            quaternion.Quaternion (0, *state ['local'] [0]),
            quaternion.Quaternion (0, *state ['local'] [1]),
            quaternion.Quaternion (0, *state ['local'] [2]),
        )

        del self.__position
        self.__position = quaternion.Quaternion (0, *state ['position'])

        del self.__distance
        self.__distance = state ['distance']

        del self.__radians
        self.__radians = state ['radians']

        del self.__stack
        if 'stack' in state:
            self.__stack = state ['stack']
        else:
            self.__stack = []

        return old

if __name__ == '__main__':

    print ('\n'.join ([
         'Tortoise3D module - Version 0.3 - 2014-06-03'
        ,''
        ,'Copyright 2008, 2014 Stefan Goebel <tortoise3d |at| subtype |dot| de>'
        ,''
        ,'This file is part of the Tortoise3D package for Python.'
        ,'' 
        ,'Tortoise3D is free software: you can redistribute it and/or modify it under the terms'
        ,'of the GNU General Public License as published by the Free Software Foundation, either'
        ,'version 3 of the License, or (at your option) any later version.'
        ,''
        ,'Tortoise3D is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;'
        ,'without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.'
        ,'See the GNU General Public License for more details.'
        ,''
        ,'You should have received a copy of the GNU General Public License along with Tortoise3D.'
        ,'If not, see <http://www.gnu.org/licenses/>.'
    ]))

# :indentSize=4:tabSize=4:noTabs=true:mode=python:maxLineLen=100: