.PHONY : all clean distclean test testclean

all:
	python setup.py bdist
	python setup.py bdist_egg
	python setup.py sdist

clean:
	rm -rf build/
	rm -rf Tortoise3D.egg-info/
	find . -type f -iname '*.pyc' -delete
	find . -type d -iname '__pycache__' -delete

distclean: clean
	rm -rf dist/

test:
	tox

testclean:
	rm -rf .tox/
